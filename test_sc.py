from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object, c_int, POINTER
from pyasi import DFT_C_API, init_dftbp, init_aims
import numpy as np
from numpy.testing import assert_allclose
from ase.build import molecule
from ase import units
import os, sys
from time import sleep
from scipy.spatial import KDTree
from scipy.interpolate import RBFInterpolator as interpolator

def filter_nearcore(cores, coords, r = 1.0):
  D = np.min(np.linalg.norm(cores[None,:, :] - coords[:, None, :], axis=-1), axis=-1)
  return D > r

def fit_ch(cores, coords, esp):
  Y = 1.0/np.linalg.norm(cores[None,:, :] - coords[:, None, :], axis=-1)
  #esp = Y @ ch
  ch = np.linalg.inv(Y.T @ Y) @ Y.T @ esp
  return ch
  
@CFUNCTYPE(None, c_void_p, c_int, POINTER(c_double), POINTER(c_double), POINTER(c_double))
def ext_pot_points(ptr, n, coords_ptr, potential_ptr, potential_grad_ptr):
    obj = cast(ptr, py_object).value
    points, ch = obj

    coords = np.ctypeslib.as_array(coords_ptr, shape=(n, 3))
    V = points[:,None, :] - coords[None, :, :]
    Y = 1.0/np.linalg.norm(V, axis=-1)
    
    if potential_ptr:
      potential = np.ctypeslib.as_array(potential_ptr, shape=(n, ))
      potential[:] = ch @ Y 

    if potential_grad_ptr:
      potential_grad = np.ctypeslib.as_array(potential_grad_ptr, shape=(n, 3))
      V *= (Y**3)[:,:,None]
      potential_grad[:] = np.einsum('i,ijk->jk', ch, V,)


def get_pnt_potential(asi, target_pos=None):
  ch = asi.atomic_charges
  positions = asi.atoms.positions / units.Bohr
  return ext_pot_points, (positions, ch)

def get_fit_pnt_potential(asi, target_pos=None):
  #ch = asi.atomic_charges
  cores = asi.atoms.positions / units.Bohr
  esp, coords = asi.get_esp()
  ext_pot_corr = asi.calc_ext_pot(coords)
  esp -= ext_pot_corr
  i = filter_nearcore(cores, coords, r = 6.0)
  ch = fit_ch(cores, coords[i], esp[i])
  print ('fitted ch:', ch, sum(i))
  return ext_pot_points, (cores, ch)


def mix_pnt_potential(pot1, pot2):
  _, (pos1, ch1) = pot1
  _, (pos2, ch2) = pot2
  assert_allclose(pos1, pos2)
  return ext_pot_points, (pos1, (ch1 + ch2)*0.5 )


@CFUNCTYPE(None, c_void_p, c_int, POINTER(c_double), POINTER(c_double), POINTER(c_double))
def ext_pot_nearest(ptr, n, coords_ptr, potential_ptr, potential_grad_ptr):
    tree, esp, esp_grad = cast(ptr, py_object).value
    coords = np.ctypeslib.as_array(coords_ptr, shape=(n, 3))
    d,i = tree.query(coords, 1, eps=0.001)
    assert np.all(d < 1e-6), d 
    
    if potential_ptr:
      potential = np.ctypeslib.as_array(potential_ptr, shape=(n, ))
      potential[:] = esp[i]

    if potential_grad_ptr:
      potential_grad = np.ctypeslib.as_array(potential_grad_ptr, shape=(n, 3))
      potential_grad[:] = esp_grad[i]

def get_tree_potential(asi, target_pos=None):
  tree = KDTree(target_pos)
  esp, esp_grad = asi.calc_esp(target_pos)
  return ext_pot_nearest, (tree, esp.copy(), esp_grad.copy())

def mix_tree_potential(pot1, pot2):
  _, (tree, esp1, esp_grad1) = pot1
  _, (tree, esp2, esp_grad2) = pot2
  return ext_pot_nearest, (tree, (esp1 + esp2)*0.5, (esp_grad1 + esp_grad2)*0.5)
 
ext_pot_func_CNT = 0

@CFUNCTYPE(None, c_void_p, c_int, POINTER(c_double), POINTER(c_double), POINTER(c_double))
def ext_pot_func(ptr, n, coords_ptr, potential_ptr, potential_grad_ptr):
  global ext_pot_func_CNT
  print ('ext_pot_func', n, ext_pot_func_CNT)
  ext_pot_func_CNT += 1
  func, _, _ = cast(ptr, py_object).value
  coords = np.ctypeslib.as_array(coords_ptr, shape=(n, 3))
  potential = np.ctypeslib.as_array(potential_ptr, shape=(n, ))  
  potential[:] = func(coords)

def get_rdf_potential(asi, target_pos=None):
  coords, esp, esp_grad = asi.get_esp()
  calc_esp, calc_esp_grad = asi.calc_esp(coords)
  print ('esp[:10,]:')
  print (esp[:10,])
  print ('calc_esp[:10,]:')
  print (calc_esp[:10,])
  
  cores = asi.atoms.positions / units.Bohr
  indices = np.min(np.linalg.norm(coords[:,None,:] - cores[None, :, :], axis=-1), axis=-1) > 1.0
  func = interpolator(coords[indices], esp[indices], neighbors=128, smoothing=0.0)

  return ext_pot_func, (func, esp, coords)

def mix_rdf_potential(pot1, pot2):
  _, (_, esp1, coords1) = pot1
  _, (_, esp2, coords2) = pot2
  assert_allclose(coords1, coords2)
  esp = (esp1+esp2) * 0.5
  func = interpolator(coords1, esp, neighbors=128, smoothing=0.0)
  return ext_pot_func, (func, esp, coords1)

tests = dict(
  dftbp_atomic_charges = (init_dftbp, './asidftbp.so', get_pnt_potential, mix_pnt_potential),
  dftbp_tree = (init_dftbp, './asidftbp.so', get_tree_potential, mix_tree_potential),
  aims_atomic_charges=(init_aims, '/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so', get_pnt_potential, mix_pnt_potential),
  aims_rdf=(init_aims, '/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so', get_rdf_potential, mix_rdf_potential),
  aims_fit_atomic_charges=(init_aims, '/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so', get_fit_pnt_potential, mix_pnt_potential),
)

if len(sys.argv) != 3:
  print(f'Usage: {sys.argv[0]} <{tests.keys()}> <distance>')
  sys.exit(1)

initializer, lib_file, get_pot, mixer = tests[sys.argv[1]]
d = float(sys.argv[2])

h2o1 = molecule('H2O')
h2o2 = h2o1.copy()
h2o2.translate([0, 0, d])

with DFT_C_API(lib_file, initializer, MPI.COMM_WORLD, h2o1 + h2o2, 'asi0') as asi0:
  asi0.run()
  E0 = asi0.total_energy
  ch0 = asi0.atomic_charges
  #esp, coords = asi0.get_esp()
  print ('F0=',np.round(asi0.total_forces,6) )
print ('E0=', E0)

with DFT_C_API(lib_file, initializer, MPI.COMM_WORLD, h2o1, 'asi1') as asi1:
  asi1.run()
  E01 = asi1.total_energy
  ch01 = asi1.atomic_charges
  pot1 = get_pot(asi1, h2o2.positions / units.Bohr)
  #print ('F1=',np.round(asi1.total_forces,6) )
print ('E01=', E01)

with DFT_C_API(lib_file, initializer, MPI.COMM_WORLD, h2o2, 'asi2') as asi2:
  asi2.run()
  E02 = asi2.total_energy
  ch02 = asi2.atomic_charges
  pot2 = get_pot(asi2, h2o1.positions / units.Bohr)
  #print ('F2=',np.round(asi2.total_forces,6) )
print ('E02=', E02)

#sys.exit(0)
for i in range (10):
  
  
  with DFT_C_API(lib_file, initializer, MPI.COMM_WORLD, h2o1, 'asi1') as asi1:
    asi1.register_external_potential(*pot2)
    asi1.run()
    new_pot1 = get_pot(asi1, h2o2.positions / units.Bohr)
    E1 = asi1.total_energy
    ch1 = asi1.atomic_charges
    f1=asi1.total_forces.copy()
   
  with DFT_C_API(lib_file, initializer, MPI.COMM_WORLD, h2o2, 'asi2') as asi2:
    asi2.register_external_potential(*pot1)
    asi2.run()
    new_pot2 = get_pot(asi2, h2o1.positions / units.Bohr)
    E2 = asi2.total_energy
    ch2 = asi2.atomic_charges
    f2=asi2.total_forces.copy()

  pot1 = mixer(pot1, new_pot1)
  pot2 = mixer(pot2, new_pot2)
  print (d, E0 - E01 - E02, E1 - E01, E2 - E02, 'raw:', E0, E01, E02, E1, E2, ch0, ch01, ch02, ch1, ch2)
  print (np.round(f1,6))
  print (np.round(f2,6))
  

