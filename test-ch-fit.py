from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object
from pyasi import DFT_C_API, init_dftbp, init_aims
import numpy as np
from ase.build import molecule
import os, sys
from time import sleep
from ase import units

@CFUNCTYPE(c_double, c_void_p, c_double, c_double, c_double)
def ext_pot_points(ptr, x,y,z):
    '''
      x,y,z : coordinates in Bohr's
      returns : potential in Ha/e
    '''
    obj = cast(ptr, py_object).value
    #print ('obj', obj)
    multipole_points, multipole_charges = obj
    p = np.array([x,y,z])
    return  np.sum(multipole_charges / ( np.linalg.norm(multipole_points - p[None, :], axis=1)))

@CFUNCTYPE(c_double, c_void_p, c_double, c_double, c_double)
def ext_pot_homog(ptr, x,y,z):
    '''
      x,y,z : coordinates in Bohr's
      returns : potential in Ha/e
    '''
    field = cast(ptr, py_object).value
    p = np.array([x,y,z])
    return  np.dot(p, field)

def plot_esp(asi):
  import matplotlib.pyplot as plt
  esp, coords = asi.get_esp()

  i = filter_nearcore(asi.atoms.positions / units.Bohr, coords)
  #print (sum(i))
  coords = coords[i]
  esp = esp[i]

  i = np.abs(coords[:, 0])< 0.1 # only Oyz plane
  #print (sum(i))
  coords = coords[i]
  esp = esp[i]

  fig = plt.figure(); ax1 = fig.add_subplot()
  ax1.scatter(coords[:,1], coords[:,2], c=esp, s=5)
  plt.savefig('esp.eps', format='eps')
  #plt.show()

def filter_nearcore(cores, coords, r = 1.0):
  D = np.min(np.linalg.norm(cores[None,:, :] - coords[:, None, :], axis=-1), axis=-1)
  return D > r

def pnt_pot(cores, ch, coords):
  Y = 1.0/np.linalg.norm(cores[None,:, :] - coords[:, None, :], axis=-1)
  esp = Y @ ch
  print (esp.shape)
  return esp

def fit_ch(cores, coords, esp):
  Y = 1.0/np.linalg.norm(cores[None,:, :] - coords[:, None, :], axis=-1)
  #esp = Y @ ch
  ch = np.linalg.inv(Y.T @ Y) @ Y.T @ esp
  return ch
  

test_cases = {'aims':   ('/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so', init_aims), 
              'dftbp':  ('./asidftbp.so', init_dftbp)}

lib_file, init_func = test_cases[sys.argv[1]]

with DFT_C_API(lib_file, init_func, MPI.COMM_WORLD, molecule('H2O'), 'asi-2h2o-1') as asi:
  asi.run()
  E0 = asi.total_energy
  ch = asi.atomic_charges
  print (f'E = {E0}, ch = {ch}')
  esp, coords = asi.get_esp()
  print (f'{coords.shape} {esp.shape} {esp[:3]} {coords[:3]}')
  cores = asi.atoms.positions / units.Bohr

  i = filter_nearcore(cores, coords, r = 4.0)
  ch_f = fit_ch(cores, coords[i], esp[i])
  print(f'ch_f = {ch_f}')
  
  esp_pf = pnt_pot(cores, ch_f, coords)
  esp_p = pnt_pot(cores, ch, coords)
  plot_esp(asi)
  i = filter_nearcore(cores, coords)
  print (sum(i))
  coords = coords[i]
  esp = esp[i]
  esp_p = esp_p[i]
  esp_pf = esp_pf[i]
  

  i = np.argsort(esp)
  np.savetxt('1.txt',np.array([esp[i], esp_p[i], esp_pf[i]]).T)

