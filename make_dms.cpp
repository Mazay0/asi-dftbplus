#include <complex>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <asi.h>

#include <mpi.h>

#include <cassert>
#include <cmath>
#include <cstdint>
#include <ctime>


#include "codespec.hpp"
#include "utils.hpp"

#define    DTYPE_             0                   /* Descriptor Type */
#define    CTXT_              1                     /* BLACS context */
#define    M_                 2             /* Global Number of Rows */
#define    N_                 3          /* Global Number of Columns */
#define    MB_                4                 /* Row Blocking Size */
#define    NB_                5              /* Column Blocking Size */
#define    RSRC_              6            /* Starting Processor Row */
#define    CSRC_              7         /* Starting Processor Column */
#define    LLD_               8           /* Local Leading Dimension */
#define    DLEN_              9                 /* Descriptor Length */

extern "C" void blacs_get_(int*, int*, int*);
extern "C" void blacs_gridinit_(int*, char*, int*, int*); // BLACS_GRIDINIT( ICONTXT, ORDER, NPROW, NPCOL )

// https://www.intel.com/content/www/us/en/develop/documentation/onemkl-developer-reference-c/top/scalapack-routines/scalapack-redistribution-copy-routines/p-gemr2d.html
// http://www.netlib.org/scalapack/slug/node164.html
// http://www.netlib.org/scalapack/slug/node168.html
extern "C" void pdgemr2d_(int *m , int *n , double *a , int *ia , int *ja , int *desca , double *b , int *ib , int *jb , int *descb , int *ictxt);
extern "C" void descinit_(int*, int*, int*, int*, int*, int*, int*, int*, int*, int*); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO

extern "C" void blacs_gridinfo_(int *, int *, int *, int *, int *); // blacs_gridinfo (icontxt, nprow, npcol, myrow, mycol);
extern "C" int numroc_(int *n, int *nb, int *iproc, int *isrcproc, int *nprocs);


int mpi_provided_threading, world_size, world_rank;


class shut_cout_t
{
  std::ofstream devnull;
  std::streambuf *coutbuf;
  public:
  shut_cout_t()
  {
    coutbuf = std::cout.rdbuf();
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    if (world_rank !=0 )
    {
      devnull.open("/dev/null");
      std::cout.rdbuf(devnull.rdbuf());
    }
  }
  
  ~shut_cout_t()
  {
    std::cout.rdbuf(coutbuf);
  }
};

int get_system_context(int blacs_context)
{
  int what=10, val;
  blacs_get_(&blacs_context, &what, &val); // get system context of the blacs_context
  return val;
}

int get_default_system_context()
{
  int system_context, zero=0;
  blacs_get_(&zero, &zero, &system_context); // get default system context
  return system_context;
}

int make_matrix_context(int init_context, int MP, int NP)
{
  char order = 'R';
  blacs_gridinit_(&init_context, &order, &MP, &NP);
 
  return init_context;
}

int locrow(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[M_], &desc[MB_], &myrow, &desc[RSRC_], &nprow);
}

int loccol(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[N_], &desc[NB_], &mycol, &desc[CSRC_], &npcol);
}

std::vector<int> make_desc(int M, int N, int ctx)
{
  std::vector<int> desc(DLEN_);
  
  if (ctx  == -1)
  {
    desc[CTXT_] = -1;
    return desc; // stub for non-participating process
  }
  
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&ctx, &nprow, &npcol, &myrow, &mycol);  
  
  int MB=1, NB=1, rsrc=0, csrc=0;
  int zero = 0;
  int info;
  
  int locrM = numroc_(&M, &MB, &myrow, &rsrc, &nprow);
  int lld = std::max(1, locrM);

  descinit_(&desc[0], &M, &N, &MB, &NB,  &rsrc,  &csrc, &ctx,  &lld, &info); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO

  return desc;
}

std::vector<double> gather(int *desc, double *data, int out_context)
{
  int N1 = 1;
  const size_t G_size = (out_context != -1) ? desc[M_]*desc[N_] : 0;
  std::vector<double> G(G_size);
  
  auto descG = make_desc(desc[M_], desc[N_], out_context);

  pdgemr2d_(&desc[M_], &desc[N_], data,     &N1, &N1, desc,
                                  G.data(), &N1, &N1, &descG[0], &desc[CTXT_]);
  return G;
}

template<typename T>
void save_txt(const char *fn, T *data, size_t M, size_t N)
{
  std::ofstream of(fn);
  for (size_t i = 0; i < M; ++i)
  {
    for (size_t j = 0; j < N; ++j)
    {
      of << data[i*N + j] << " ";
    }
    of << std::endl;
  }
}


void dm_callback(void *aux_ptr, int iK, int iS, int *blacs_descr, void *blacs_data)
{
  std::cout << "dm_callback @" << world_rank << " = " << locrow(blacs_descr) << "/" << blacs_descr[M_] << " X " << loccol(blacs_descr) << "/" << blacs_descr[N_] << std::endl;
  if (world_rank == 0)
  {
    double *data = reinterpret_cast<double *>(blacs_data);
    save_txt("DM.txt", data, locrow(blacs_descr), loccol(blacs_descr));
  }
}

int S_cnt = 0;
void s_callback(void *aux_ptr, int *blacs_descr, void *blacs_data)
{
  std::cout << "S_callback @" << world_rank << " = " << locrow(blacs_descr) << "/" << blacs_descr[M_] << " X " << loccol(blacs_descr) << "/" << blacs_descr[N_] << std::endl;
  if (world_rank == 0)
  {
    double *data = reinterpret_cast<double *>(blacs_data);
    save_txt(STR("S_"<<S_cnt<<".txt").c_str(), data, locrow(blacs_descr), loccol(blacs_descr));
  }
  S_cnt++;
}

int H_cnt = 0;
void h_callback(void *aux_ptr, int *blacs_descr, void *blacs_data)
{
  std::cout << "H_callback @" << world_rank << " = " << locrow(blacs_descr) << "/" << blacs_descr[M_] << " X " << loccol(blacs_descr) << "/" << blacs_descr[N_] << std::endl;
  if (world_rank == 0)
  {
    double *data = reinterpret_cast<double *>(blacs_data);
    save_txt(STR("H_"<<H_cnt<<".txt").c_str(), data, locrow(blacs_descr), loccol(blacs_descr));
  }
  H_cnt++;
}

int main(int argc, char *argv[])
{
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_provided_threading); // instead of MPI_Init
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
 // shut_cout_t shut_cout_instance;
 
  assert(argc == 10);
  

  MPI_Barrier(MPI_COMM_WORLD);
  
  const MPI_Fint f_mpi_comm = MPI_Comm_c2f(MPI_COMM_WORLD);

  make_config_files(false);
  
  ASI_init("asi.log", f_mpi_comm);
  std::cout << "Number of atoms == " << ASI_n_atoms() << std::endl;
  std::cout << "ASI_get_nspin == " << ASI_get_nspin() << std::endl;
  std::cout << "ASI_get_nkpts == " << ASI_get_nkpts() << std::endl;
  std::cout << "ASI_is_hamiltonian_real == " << ASI_is_hamiltonian_real() << std::endl;
  std::cout << "ASI_get_basis_size == " << ASI_get_basis_size() << std::endl;
  std::cout << "ASI_get_n_local_ks == " << ASI_get_n_local_ks() << std::endl;
  
  std::vector< int > local_ks(ASI_get_n_local_ks() * 2);
  ASI_get_local_ks(local_ks.data());
  std::cout << "local_ks @" << world_rank << " : ";
  for(int ks:local_ks)
  {
    std::cout << ks << " ";
  }
  std::cout << std::endl;

  ASI_register_dm_callback(dm_callback, 0);
  ASI_register_overlap_callback(s_callback, 0);
  ASI_register_hamiltonian_callback(h_callback, 0);
  
  double coords[9];
  for (int i = 0; i < 9; ++i)
  {
    coords[i] = std::stod(argv[1+i]);
  }
  for (auto &x : coords)
  {
    x /= 0.52917721;
  }
  /// ASI_set_atom_coords(coords, 3);

  assert(ASI_is_hamiltonian_real());
  std::cout << "ASI_run() ..." << std::endl;
  auto t0 = rdtsc();
  ASI_run();
  std::cout << "ASI_run() DONE!" << std::endl;
  
  auto E = ASI_energy();
  std::cout << std::setprecision(6) << std::fixed;
  std::cout << "Energy == " << E  << " Ha = " << E * 27.2113845 << " eV" << std::endl;
  
  const double *forces = ASI_forces();
  std::cout << "Forces == ";
  for (size_t i = 0; i < 3 * ASI_n_atoms(); ++i )
    std::cout << forces[i] << " ";

  const double *atomic_charges = ASI_atomic_charges();
  std::cout << "atomic_charges == ";
  for (size_t i = 0; i < ASI_n_atoms(); ++i )
    std::cout << atomic_charges[i] << " ";
  std::cout << std::endl;

  MPI_Barrier(MPI_COMM_WORLD);
  
  
  /*
  std::vector<int> local_ks(ASI_get_nkpts()*ASI_get_nspin());
  int n_local_ks = ASI_get_local_ks(local_ks.data());
  std::cout << "n_local_ks = " << n_local_ks  << " @ " << world_rank << std::endl;
  for (auto ki: local_ks)
  {
    std::cout << "ki in local_ks = " << ki << " @ " << world_rank << std::endl;
  }
  
  test_dm_export();*/
  
  ASI_finalize(); 
  MPI_Finalize();
  return 0;
}
