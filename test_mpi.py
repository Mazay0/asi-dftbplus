from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object, c_int, POINTER
from pyasi import DFT_C_API, init_dftbp, init_aims
import numpy as np
from numpy.testing import assert_allclose
from ase.build import molecule
from ase import units
import os, sys
from time import sleep
from itertools import product

coords = np.zeros((3,3), dtype=np.float64)
print ('Send:',coords.dtype)
if MPI.COMM_WORLD.Get_rank() == 0:
  MPI.COMM_WORLD.Send(coords, dest=1, tag=1)
else:
  MPI.COMM_WORLD.Recv(coords, source=0, tag=1)

