import sys, os
import numpy as np
from ctypes import cast, py_object
import dmpredict

from ase.build import molecule
from ase.io import read, write
from pyasi.asecalc import ASI_ASE_calculator
from ase.md.verlet import VelocityVerlet
from ase import units

os.environ["OMP_NUM_THREADS"]="1"
os.environ["ASE_AIMS_COMMAND"]="mpiexec -n 4 /home/mazay/prg/aims/build-exe/aims.220309.scalapack.mpi.x | tee -a aims.total.log.3"
os.environ["AIMS_SPECIES_DIR"]="/home/mazay/prg/aims/species_defaults/defaults_2010/tight"
cwd = os.getcwd()

def make_aims_calc(iPI=False):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pw-lda', 
    sc_accuracy_forces=1e-1, # just to enable force calculation
    species_dir=os.environ["AIMS_SPECIES_DIR"],
    density_update_method="density_matrix",
    #elsi_restart="read_and_write 1",
    #elsi_restart="read 1",
    tier = [1, 2],
  )
  if iPI:
    calc.parameters["use_pimd_wrapper"]=("localhost", 12346)
  return calc

def make_socket_calc():
  from ase.calculators.socketio import SocketIOCalculator
  aims_calc = make_aims_calc(True)
  return SocketIOCalculator(aims_calc, log="socket_calc.log", port=12346)

def init_aims(asi):
  make_aims_calc().write_input(asi.atoms)
  
def s_calc(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  try:
    if descr:
      descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
      if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
        print("distributed case not implemented")
        return # TODO distributed case not implemented
      else:
        pass
    else:
      pass
    # single process case:
    print (f"s_calc invoked {asi.scf_cnt}")
    data = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis))
    asi.overlap = data.copy()
    np.savetxt(f"s.txt", asi.overlap)
  except Exception as eee:
    print ("Something happened in s_calc", eee)

  
def dm_calc(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  try:
    if descr:
      descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
      if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
        print("distributed case not implemented")
        return # TODO distributed case not implemented
      else:
        pass
    else:
      pass
    # single process case:
    asi.scf_cnt += 1
    #print (f"dm_calc invoked {asi.scf_cnt}")
    data = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis))
    np.savetxt(f"dm_{asi.scf_cnt}.txt", data)
    np.savetxt(f"s_{asi.scf_cnt}.txt", asi.overlap)
    #print (f"S*D = {np.sum(data * asi.overlap)}")
  except Exception as eee:
    print ("Something happened in dm_calc", eee)


  
def dm_init(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  print ("dm_init")
  try:
    if descr:
      descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
      if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
        print("distributed case not implemented")
        return # TODO distributed case not implemented
      else:
        pass
    else:
      pass
    # single process case:
    data = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis))
    data[:,:] = dmpredict.predict(cwd, asi.atoms.copy(), ) # asi.overlap
    #data[:,:] = np.loadtxt("../dm.txt")
    print ("dm predict done")
  except:
    print ("Something happened in dm_init")

def dyn_step(asi):
  print (f"dyn_step asi.scf_cnt = {asi.scf_cnt}")
  asi.scf_cnt = 0
  #atoms.calc.asi.register_DM_init(dm_init, atoms.calc.asi)


atoms = molecule("H2O", vacuum=6)
atoms.positions[0,2] += 0.2


##
## Select calculator here:
##
atoms.calc = ASI_ASE_calculator(f"{os.environ['HOME']}/prg/aims/build-so-3/libaims.220309.scalapack.mpi.so", init_aims, None, atoms)
atoms.calc.asi.scf_cnt = 0
atoms.calc.asi.register_DM_init(dm_init, atoms.calc.asi)
atoms.calc.asi.register_dm_callback(dm_calc, atoms.calc.asi)
atoms.calc.asi.register_overlap_callback(s_calc, atoms.calc.asi)
#atoms.calc = make_socket_calc()
#atoms.calc = make_aims_calc()

np.set_printoptions(precision=30)
print (atoms.get_potential_energy())
print (atoms.get_forces())

dyn = VelocityVerlet(atoms, 1.0 * units.fs, trajectory = "1.traj")
dyn.attach(lambda :dyn_step(atoms.calc.asi), interval=1)
dyn.run(100)



