import numpy as np
import sys
import matplotlib.pyplot as plt

S = np.loadtxt(sys.argv[1])
fig, ax = plt.subplots()
im = ax.imshow(S)
plt.show()

