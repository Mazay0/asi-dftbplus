import sys
import os
import numpy as np
from ctypes import c_double, c_int, cast, py_object, POINTER, c_void_p, c_char_p, Structure, CFUNCTYPE, byref
from mpi4py import MPI
from mpi4py.util.dtlib import from_numpy_dtype
#print ('np.dtype(c_double)', np.dtype(c_double).descr)
mpitype_d = from_numpy_dtype(np.dtype(c_double))
mpitype_d.Commit()

from ase.build import molecule
from ase.parallel import parprint
from ase.io import read, write
from pyasi.pyasi import init_aims, DFT_C_API, DLEN_

import dmpredict

from ctypes import cdll

class aims_initializer:
  def __init__(self, forces=False, charges=False, init_dm=False):
    self.forces = forces
    self.charges = charges
    self.init_dm = init_dm
  def __call__(self, asi):
    from ase.calculators.aims import Aims
    species = set(asi.atoms.symbols)
    tier_map = {'O':1, 'H':2}
    tier = [tier_map[s] for s in species]
    tier = [1, 2]
    calc = Aims(xc='pbe', 
      relativistic="atomic_zora scalar",
      occupation_type="gaussian 0.010",
      sc_accuracy_eev=1E-3,
      sc_accuracy_rho=1e-05,
      sc_accuracy_etot=1e-06,
      charge_mix_param=0.20,
      mixer="pulay",
      n_max_pulay=8,
      preconditioner="kerker 1.000",
      kerker_factor=1.0,
      KS_method="elpa",
      tier = tier,
      density_update_method="density_matrix",
      #species_dir='/home/mazay/prg/aims/species_defaults/defaults_2020/light'
      species_dir='/home/mazay/prg/aims/species_defaults/defaults_2010/tight'
    )
    if self.forces:
      calc.parameters["compute_forces"]=True
    if self.charges:
      calc.parameters['output']=['hirshfeld-I']
    if self.init_dm:
      calc.parameters["elsi_restart"]="read"

    calc.write_input(asi.atoms)


cwd = os.getcwd()

atoms=read("h2o_dimer.xyz")
#atoms = molecule("H2O")
#atoms.translate([0,0,9])
#atoms += molecule("H2O")
write("1.xyz", [atoms], "extxyz")
#seed = int(sys.argv[1])
#atoms.rattle(0.10 , seed)
#write(f"rattle_30_{seed}.xyz",[atoms], "extxyz")

#dmpredict.predict(cwd, atoms.copy()) # , asi.overlap
#sys.exit(0)

def dm_init(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  print ("dm_init")
  if descr:
    descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
    if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
      print("distributed case not implemented")
      return # TODO distributed case not implemented
    else:
      pass
  else:
    pass
  # single process case:
  data = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis))
  data[:,:] = 0
  data[:44,:44] = dmpredict.predict(cwd, asi.atoms[:3].copy()) # asi.overlap[:44, :44]
  data[44:,44:] = dmpredict.predict(cwd, asi.atoms[3:].copy()) # , asi.overlap[44:, 44: ]
  #data[:44,:44] = np.loadtxt(f"{cwd}/dm_1_1.init")
  #data[44:,44:] = np.loadtxt(f"{cwd}/dm_1_1.init")
  #dm = np.loadtxt(f"{cwd}/dm_calc_88.txt")
  #data[:44,:44] = dm[:44,:44]
  #data[44:,44:] = dm[44:,44:]

def overlap_callback(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  print ("overlap_callback")
  if descr:
    descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
    if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
      print("distributed case not implemented")
      return # TODO distributed case not implemented
    else:
      pass
  else:
    pass
  # single process case:
  asi.overlap = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis)).copy()
  np.savetxt("S_calc.txt", asi.overlap)

def dm_callback(aux, iK, iS, descr, data):
  asi = cast(aux, py_object).value
  print ("dm_callback")
  if descr:
    descr = np.ctypeslib.as_array(descr, shape=(DLEN_,))
    if descr[2] != descr[8]:  # M != LLD, so matrix is distributed
      print("distributed case not implemented")
      return # TODO distributed case not implemented
    else:
      pass
  else:
    pass
  # single process case:
  dm = np.ctypeslib.as_array(data, shape=(asi.n_basis,asi.n_basis)).copy()
  np.savetxt("dm_calc_88.txt", dm)

#/home/mazay/prg/aims/build-so-3/libaims.220225.scalapack.mpi.so
with DFT_C_API("/home/mazay/prg/aims/build-so-3/libaims.220309.scalapack.mpi.so", aims_initializer(False, False, False), MPI.COMM_WORLD, atoms) as asi:
  parprint("n_basis = ", asi.n_basis)
  asi.register_DM_init(dm_init, asi)
  asi.register_overlap_callback(overlap_callback, asi)
  asi.register_dm_callback(dm_callback, asi)
  asi.run()
  parprint("asi.total_energy = ", asi.total_energy)

