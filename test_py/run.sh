#!/bin/sh

ulimit -s unlimited

export PYTHONPATH=$PYTHONPATH:`pwd`/../python/

mpiexec -n 1 ~/ve/bin/python -u test1.py $1

grep -e 'End self' asi.temp/asi.log  | tail -n 1
