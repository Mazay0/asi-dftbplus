import sys
import os
import numpy as np

from ase.build import molecule
from ase.io import read, write
from pyasi.asecalc import ASI_ASE_calculator
from ase.md.verlet import VelocityVerlet
from ase.md.andersen import Andersen
from ase.optimize.lbfgs import LBFGS
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase import units
from itertools import product
import sys

os.environ["ASE_AIMS_COMMAND"]="mpiexec -n 4 /home/mazay/prg/aims/build-exe/aims.220309.scalapack.mpi.x"
os.environ["AIMS_SPECIES_DIR"]="/home/mazay/prg/aims/species_defaults/defaults_2020/light"

def make_aims_calc():
  from ase.calculators.aims import Aims
  return Aims(xc='pw-lda', 
    sc_accuracy_forces=1e-1, # just to enable force calculation
    species_dir=os.environ["AIMS_SPECIES_DIR"],
    elsi_restart="read_and_write 1"
  )

def init_aims(asi):
  make_aims_calc().write_input(asi.atoms)
  
def init_dftbp(asi):
  from ase.calculators.dftb import Dftb
  Dftb(label='Some_cluster', slako_dir='/home/mazay/prg/slakos/origin/mio-1-1/',
        Hamiltonian_SCC='Yes',
        kpts=[1,1,1],
        Hamiltonian_MaxAngularMomentum_='',
        Hamiltonian_MaxAngularMomentum_O='"p"',
        Hamiltonian_MaxAngularMomentum_H='"s"').write_input(asi.atoms, properties=['forces'])


if False:
  atoms = []
  #atoms.translate positions[0,2] += 0.2
  for ixyz in product(range(4), repeat=3):
    w = molecule("H2O", vacuum=6)
    w.translate(np.array(ixyz) * 2.5 )
    atoms += [w]

  atoms = sum(atoms[1:],atoms[0])
  atoms.cell = [5*4]*3
  atoms.pbc = True
  atoms.rattle(0.05)
  #MaxwellBoltzmannDistribution(atoms, temperature_K=350)
  write("1.xyz", [atoms], "extxyz")

  atoms.calc = ASI_ASE_calculator("/home/mazay/prg/asi-dftbp/asidftbp.so", init_dftbp, None, atoms)
  opt = LBFGS(atoms, trajectory = "1.traj")
  opt.run(fmax=1.0)
else:
  atoms = read('watermd.traj', index=-1)
  atoms.calc = ASI_ASE_calculator("/home/mazay/prg/asi-dftbp/asidftbp.so", init_dftbp, None, atoms)
  
if False:
  dyn = Andersen(atoms, 1.0*units.fs, temperature_K=300, andersen_prob=0.1, trajectory='watermd.traj', append_trajectory=True)
  dyn.run(5000)
else:
  opt = LBFGS(atoms, trajectory = "opt.traj")
  opt.run(fmax=0.01)

  
sys.exit(0)

##
## Select calculator here:
##
atoms.calc = ASI_ASE_calculator("/home/mazay/prg/aims/build-so-3/libaims.220309.scalapack.mpi.so", init_aims, None, atoms)
#atoms.calc = make_aims_calc()

print (atoms.get_potential_energy())
print (atoms.get_forces())

dyn = VelocityVerlet(atoms, 0.1 * units.fs, trajectory = "1.traj")
dyn.run(100)



