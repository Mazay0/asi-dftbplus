import ase
import ase.io
import schnetpack as spk
import os
import sys
import schnorb
from schnorb.data import SchNOrbAtomsData, SchNOrbProperties
from SchNOrbCalculator import SchNOrbCalculator, AtomsData
from schnorb.rotations import AimsRotator as rot
import torch
import numpy as np
from Fermilevel import get_occupancies, determine_fermi_level

def predict(working_dir, atoms, S=None):
  # Reference: https://web-ornl-gov.translate.goog/~kentpr/thesis/pkthnode58.html?_x_tr_sl=en&_x_tr_tl=de&_x_tr_hl=de&_x_tr_pto=sc
  # https://0-iopscience-iop-org.pugwash.lib.warwick.ac.uk/article/10.1088/0953-8984/25/29/295501
  # 

  # ## Load Model and Make WF prediction

  # In[2]:




  # In[3]:


  device = "cpu"
  modelpath = f"{working_dir}/Model/"
  datapath = f"{working_dir}/DataBase/H2O_H_aims.db"
  #get training arguments
  #jsonpath = os.path.join(modelpath, 'args.json')
  #train_args = spk.utils.read_from_json(jsonpath)


  # In[4]:


  # load model
  model = torch.load(os.path.join(modelpath, 'best_model'), device)


  # In[5]:


  #generate input. Attention: only works for H2O
  atomsdata = AtomsData(datapath)
  input = atomsdata.__getitem__(atoms)
  result = model(input)


  # # Get Density Matrix 

  # In[6]:


  from ase.units import Hartree
  import scipy 

  H = result["hamiltonian"][0].detach().numpy()
  if S is None:
    S = result["overlap"][0].detach().numpy()

  #print(S[0])
  eigenvalues,eigenvectors = scipy.linalg.eigh(Hartree*H,S)
  #print(eigenvalues)


  # In[7]:


  # Occupation vector
  nelectrons = 10
  fermi_level = determine_fermi_level(eigenvalues, nelectrons,sigma=0.01)
  occupation = get_occupancies(eigenvalues,fermi_level,"gaussian",0.01)
  #print(occupation)


  # In[8]:


  dm=np.zeros((44,44))
  for i in range(len(eigenvalues)):
      dm+=occupation[i]*np.outer(eigenvectors[:,i].transpose(),eigenvectors[:,i])
  return dm

