from mpi4py import MPI

import numpy as np
from scalapack4py import ScaLAPACK4py
from mpiprint import parprint, ordprint


from ctypes import CDLL
import os
blacs = CDLL(f"{os.environ['HOME']}/prg/aims/build-so-3/libaims.220309.scalapack.mpi.so")
sl = ScaLAPACK4py(blacs)


sys_ctx = sl.get_default_system_context()
parprint(sys_ctx)
blacs_ctx1 = sl.make_blacs_context(sys_ctx, 1, 2)
parprint(blacs_ctx1)
ordprint(1, sl.blacs_gridinfo(blacs_ctx1))

desc = sl.make_blacs_desc(blacs_ctx1, 10, 10, nb=2)
ordprint (desc, desc.locrow, desc.loccol)
data = desc.alloc_zeros(np.float64)
ordprint (data.shape)

if MPI.COMM_WORLD.Get_rank() == 0:
  xv, yv = np.meshgrid(np.arange(0, 10.0), np.arange(0, 10.))
  src_grid = np.asfortranarray(xv + 100*yv)
else:
  src_grid = None
ordprint(src_grid)
sl.scatter(src_grid, desc, data)
ordprint(data)

gathered_data = np.ndarray((10, 10), dtype = data.dtype, order='F') if MPI.COMM_WORLD.Get_rank() == 0 else None
sl.gather(desc, data, gathered_data)
ordprint(gathered_data)


