from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object, c_int, POINTER
from pyasi import DFT_C_API, init_dftbp, init_aims
import numpy as np
from ase.build import molecule
import os, sys
from time import sleep

@CFUNCTYPE(None, c_void_p, c_int, POINTER(c_double), POINTER(c_double), POINTER(c_double))
def ext_pot_points(ptr, n, coords_ptr, potential_ptr, potential_grad_ptr):
    obj = cast(ptr, py_object).value
    points, ch = obj

    coords = np.ctypeslib.as_array(coords_ptr, shape=(n, 3))
    V = points[:,None, :] - coords[None, :, :]
    Y = 1.0/np.linalg.norm(V, axis=-1)
    
    if potential_ptr:
      potential = np.ctypeslib.as_array(potential_ptr, shape=(n, ))
      potential[:] = ch @ Y 

    if potential_grad_ptr:
      potential_grad = np.ctypeslib.as_array(potential_grad_ptr, shape=(n, 3))
      V *= (Y**3)[:,:,None]
      potential_grad[:] = np.einsum('i,ijk->jk', ch, V,)
      

@CFUNCTYPE(c_double, c_void_p, c_double, c_double, c_double)
def ext_pot_homog(ptr, x,y,z):
    '''
      x,y,z : coordinates in Bohr's
      returns : potential in Ha/e
    '''
    field = cast(ptr, py_object).value
    p = np.array([x,y,z])
    return  np.dot(p, field)

test_cases = {'aims':   ('/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so', init_aims), 
              'dftbp':  ('./asidftbp.so', init_dftbp)}

lib_file, init_func = test_cases[sys.argv[1]]

with DFT_C_API(lib_file, init_func, MPI.COMM_WORLD, molecule('H2O'), 'asi-2h2o-1') as asi:
  asi.run()
  E0 = asi.total_energy
  print (f'E = {E0}, ch = {asi.atomic_charges}')
  esp, coords = asi.get_esp()
  print (f'{coords.shape} {esp.shape} {esp[:3]} {coords[:3]}')
  esp, esp_grad = asi.calc_esp(np.array([[5,5,5],[10,10,10]], dtype=np.float64))
  print ('ESP = ',esp)
  print (asi.total_forces)

probe_q = 1E-3

with DFT_C_API(lib_file, init_func, MPI.COMM_WORLD, molecule('H2O'), 'asi-2h2o-2') as asi:
  asi.register_external_potential(ext_pot_points, (np.array([[5., 5, 5],]), np.array([probe_q,])))
  asi.run()
  E1 = asi.total_energy
  probe_esp = (E1 - E0) / probe_q
  err = abs(esp[0] - probe_esp)
  print (f'E1 = {E1} ')
  print (f'probed ESP = {probe_esp}, err = {err}')
  assert err < 1e-5
  print (asi.total_forces)

sys.exit(0)
'''  
for d in np.arange(4.7, 5.1, 0.1):
  atoms_h2o_1 = molecule('H2O')
  atoms_h2o_2 = molecule('H2O')
  atoms_h2o_2.translate([0, 0, d])
  h2o_pair = atoms_h2o_1 + atoms_h2o_2
  with DFT_C_API('./asidftbp.so', init_dftbp, MPI.COMM_WORLD, h2o_pair, 'asi-2h2o') as asi:
    asi.run()
    print (f'{d:.2f}  {asi.total_energy}')


'''
