#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>


#include <mpi.h>

#include <cassert>
#include <cmath>
#include <cstdint>
#include <ctime>

#define    DTYPE_             0                   /* Descriptor Type */
#define    CTXT_              1                     /* BLACS context */
#define    M_                 2             /* Global Number of Rows */
#define    N_                 3          /* Global Number of Columns */
#define    MB_                4                 /* Row Blocking Size */
#define    NB_                5              /* Column Blocking Size */
#define    RSRC_              6            /* Starting Processor Row */
#define    CSRC_              7         /* Starting Processor Column */
#define    LLD_               8           /* Local Leading Dimension */
#define    DLEN_              9                 /* Descriptor Length */

extern "C" void blacs_get_(int*, int*, int*);
extern "C" void blacs_gridinit_(int*, char*, int*, int*); // BLACS_GRIDINIT( ICONTXT, ORDER, NPROW, NPCOL )

// https://www.intel.com/content/www/us/en/develop/documentation/onemkl-developer-reference-c/top/scalapack-routines/scalapack-redistribution-copy-routines/p-gemr2d.html
// http://www.netlib.org/scalapack/slug/node164.html
// http://www.netlib.org/scalapack/slug/node168.html
extern "C" void pdgemr2d_(int *m , int *n , double *a , int *ia , int *ja , int *desca , double *b , int *ib , int *jb , int *descb , int *ictxt);
extern "C" void descinit_(int*, int*, int*, int*, int*, int*, int*, int*, int*, int*); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO

extern "C" void blacs_gridinfo_(int *, int *, int *, int *, int *); // blacs_gridinfo (icontxt, nprow, npcol, myrow, mycol);
extern "C" int numroc_(int *n, int *nb, int *iproc, int *isrcproc, int *nprocs);


int mpi_provided_threading, world_size, world_rank;


template<typename T>
void print_enumerated(size_t n, const T *v)
{
  for(size_t i = 0; i < n; ++i)
  {
    std::cout << i << " " << v[i] <<", ";
  }
  std::cout << " @ " << world_rank << std::endl;
}

void save_txt(const char *fn, size_t n, const double *v)
{
  std::ofstream fo(fn);
  fo << std::setprecision(15);
  fo <<  n << std::endl;
  for(size_t i = 0; i < n; ++i)
  {
    fo << v[i] << "\n";
  }
  fo.close();
}

std::vector<double> load_txt(const char *fn)
{
  std::ifstream fi(fn);
  size_t n, i;
  fi >>  n;
  std::vector<double> v(n);
  for(size_t i = 0; i < n; ++i)
  {
    fi >>  v[i];
  }
  fi.close();
  return v;
}

double maxerr(size_t n, const double *a, const double *b)
{
  double res = 0;
  for(auto i = 0; i < n; ++i)
  {
    res = std::max(res, fabs(a[i] - b[i]));
  }
  return res;
}

int get_system_context()
{
  int system_context, zero=0;
  blacs_get_(&zero, &zero, &system_context); // get default system context
  return system_context;
}

int make_matrix_context(int init_context, int MP, int NP)
{
  char order = 'C';
  blacs_gridinit_(&init_context, &order, &MP, &NP);
 
  return init_context;
}

int locrow(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[M_], &desc[MB_], &myrow, &desc[RSRC_], &nprow);
}

int loccol(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[N_], &desc[NB_], &mycol, &desc[CSRC_], &npcol);
}

std::vector<int> make_desc(int M, int N, int ctx)
{
  std::vector<int> desc(DLEN_);
  
  if (ctx  == -1)
  {
    desc[CTXT_] = -1;
    return desc; // stub for non-participating process
  }
  
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&ctx, &nprow, &npcol, &myrow, &mycol);  
  
  int MB=1, NB=1, rsrc=0, csrc=0;
  int zero = 0;
  int info;
  
  int locrM = numroc_(&M, &MB, &myrow, &rsrc, &nprow);
  std::cout << "locrM =  " << locrM << "  @ " << world_rank << " M=" << M << " MB=" <<  MB << " myrow=" <<  myrow << " rsrc=" <<  rsrc << " nprow=" <<  nprow<< std::endl;
  int lld = std::max(1, locrM);

  descinit_(&desc[0], &M, &N, &MB, &NB,  &rsrc,  &csrc, &ctx,  &lld, &info); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO
  std::cout << "make_desc locs:  " <<  locrow(&desc[0]) << " x " << loccol(&desc[0])  << "  @ " << world_rank << std::endl;

  return desc;
}

std::vector<double> gather(int *desc, double *data, int out_context)
{
  int N1 = 1;
  const size_t G_size = (world_rank == 0) ? desc[M_]*desc[N_] : 0;
  std::vector<double> G(G_size);
  
  auto descG = make_desc(desc[M_], desc[N_], out_context);

  std::cout << "gather @" << world_rank << ": " << desc[CTXT_] << " -> " << descG[CTXT_] << " common: " << desc[CTXT_] << std::endl;
  pdgemr2d_(&desc[M_], &desc[N_], data,     &N1, &N1, desc,
                                  G.data(), &N1, &N1, &descG[0], &desc[CTXT_]);
  std::cout << "gather @" << world_rank << " done"<< std::endl;
  
  return G;
}

void distribute(int M, int N, int *in_desc, double *in_data, int out_context, int global_context, std::vector<int> &out_desc, std::vector<double> &out_data)
{
  
  make_desc(M, N, out_context).swap(out_desc);
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "out_desc @ " << world_rank << "\n";
  print_enumerated(out_desc.size(), &out_desc[0]);
  MPI_Barrier(MPI_COMM_WORLD);
  
  out_data.resize(locrow(&out_desc[0]) * loccol(&out_desc[0]));
  std::cout <<  "out_data.size()=" << out_data.size() << " @ " << world_rank << std::endl;
  
  int one = 1;
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "distribute @" << world_rank << ": " << in_desc[CTXT_] << " -> " << out_desc[CTXT_] << " common: " << global_context << std::endl;
  std::cout << "distribute @" << world_rank << " M=" << M << " N=" << N <<  std::endl;
  pdgemr2d_(&M, &N, in_data, &one, &one, in_desc,
              out_data.data(),  &one, &one, &out_desc[0], &global_context);
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "distribute @" << world_rank << " done"<< std::endl;
  MPI_Barrier(MPI_COMM_WORLD);
}

void load_and_distribute(const char *fn, std::vector<int> &out_desc, std::vector<double> &out_data)
{
  std::cout << "fn " << fn << std:: endl;
  auto data = load_txt(fn);
  int M = sqrt(data.size());
  int N = M;
  std::cout << "data " << data.size() << " = " <<  M << " x " << N << std:: endl;


  int sys_ctx = get_system_context();
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&sys_ctx, &nprow, &npcol, &myrow, &mycol);
  std::cout << "sys_ctx " << sys_ctx << " nprow, npcol, myrow, mycol:" << nprow << " " << npcol << " " << myrow  << " " <<  mycol << std:: endl;
  int ctx1x1 = make_matrix_context(sys_ctx, 1, 1);
  blacs_gridinfo_(&ctx1x1, &nprow, &npcol, &myrow, &mycol);
  std::cout << "ctx1x1 " << ctx1x1 << " nprow, npcol, myrow, mycol:" << nprow << " " << npcol << " " << myrow  << " " <<  mycol << std:: endl;
  auto desc = make_desc(M, N, ctx1x1);
  std::cout << "desc 1x1" << desc[CTXT_] << std:: endl;
  
  
  int out_ctx = make_matrix_context(sys_ctx, world_size, 1);
  blacs_gridinfo_(&out_ctx, &nprow, &npcol, &myrow, &mycol);
  std::cout << "out_ctx " << out_ctx << " nprow, npcol, myrow, mycol:" << nprow << " " << npcol << " " << myrow  << " " <<  mycol << std:: endl;

  MPI_Barrier(MPI_COMM_WORLD);
  distribute(M, N, &desc[0], &data[0], out_ctx, out_ctx, out_desc, out_data);
  
  MPI_Barrier(MPI_COMM_WORLD);
  print_enumerated(out_desc.size(), &out_desc[0]);
  MPI_Barrier(MPI_COMM_WORLD);
  print_enumerated(out_data.size(), &out_data[0]);
  MPI_Barrier(MPI_COMM_WORLD);
  
  
  
  auto full_dm = gather(&out_desc[0], &out_data[0], ctx1x1);
  MPI_Barrier(MPI_COMM_WORLD);
  
  if (world_rank == 0)
  {
    assert(full_dm.size() == data.size());  
    auto err = maxerr(full_dm.size(), &full_dm[0], &data[0]); 
    std::cout << "maxerr on load_and_distribute @" << world_rank << " = " << err << std::endl;
  }
}



int main(int argc, char *argv[])
{
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_provided_threading); // instead of MPI_Init
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
 // shut_cout_t shut_cout_instance;
 /*
  int M = 3, N = 3, one=1;
  
  std::vector<double> data_in(M*N);  // upper bounds
  std::vector<double> data_out(M*N); // upper bounds
  
  for (int i = 0; i < data_in.size(); ++i)
  {
    data_in.at(i) = (i+1) * 100;
  }

  for (int i = 0; i < data_out.size(); ++i)
  {
    data_out.at(i) = (i+1) * 10;
  }
  
    
  int ctx_Mx1 = make_matrix_context(get_system_context(), world_size, 1);
  int ctx_1x1 = make_matrix_context(get_system_context(), 1, 1);
  
  auto desc_Mx1 = make_desc(M, N, ctx_Mx1);
  auto desc_1x1 = make_desc(M, N, ctx_1x1);
  
  // (int *m , int *n , 
  // double *a , int *ia , int *ja , int *desca ,
  // double *b , int *ib , int *jb , int *descb , int *ictxt);
  
  pdgemr2d_(&M, &N, 
    &data_in[0], &one, &one, &desc_Mx1[0], 
    &data_out[0], &one, &one, &desc_1x1[0], 
    &desc_Mx1[CTXT_]);

  for (int i = 0; i < world_size; ++i)
  {
    MPI_Barrier(MPI_COMM_WORLD);
    if (world_rank == i)
    {
      std::cout << "Rank " << world_rank << std::endl;
      print_enumerated(data_out.size(), &data_out[0]);
      std::cout << "desc_1x1[CTXT_] = " << desc_1x1[CTXT_] << std::endl;
      std::cout << "" << std::endl;
    }
  }
  return 0;
  */
 
  std::vector<int> dm_init_desc;
  std::vector<double> dm_init_data;
  load_and_distribute("m_init.txt", dm_init_desc, dm_init_data);
  
  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
  return 0;
}
