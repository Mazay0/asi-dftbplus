#!/bin/sh

ulimit -s unlimited

rm -r work
mkdir work
cd work

TEST_LOG=test1
mpiexec -n 1 ../../test_esp-aims | tee  $TEST_LOG
diff --color -s $TEST_LOG ../$TEST_LOG


TEST_LOG=test2
mpiexec -n 2 ../../test_esp-aims | tee  $TEST_LOG
diff --color -s $TEST_LOG ../$TEST_LOG


