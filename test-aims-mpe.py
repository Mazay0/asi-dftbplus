from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object
from pyasi import DFT_C_API
import numpy as np
from ase.build import molecule
import os, sys
from time import sleep

@CFUNCTYPE(c_double, c_void_p, c_double, c_double, c_double)
def ext_pot_points(ptr, x,y,z):
    '''
      x,y,z : coordinates in Bohr's
      returns : potential in Ha/e
    '''
    obj = cast(ptr, py_object).value
    #print ('obj', obj)
    multipole_points, multipole_charges = obj
    p = np.array([x,y,z])
    return  np.sum(multipole_charges / ( np.linalg.norm(multipole_points - p[None, :], axis=1)))

@CFUNCTYPE(c_double, c_void_p, c_double, c_double, c_double)
def ext_pot_homog(ptr, x,y,z):
    '''
      x,y,z : coordinates in Bohr's
      returns : potential in Ha/e
    '''
    field = cast(ptr, py_object).value
    p = np.array([x,y,z])
    return  np.dot(p, field)

def init_aims(asi):
  from ase.calculators.aims import Aims
  #Aims(xc='PBE', output=['hirshfeld-I'], solvent=['mpe'], isc_cavity_type=['rho_multipole_static 0.036'], species_dir='/home/mazay/prg/aims/species_defaults/defaults_2020/light').write_input(asi.atoms)
  #Aims(xc='LDA', output=['hirshfeld-I'], species_dir='/home/mazay/prg/aims/species_defaults/defaults_2020/light').write_input(asi.atoms)
  Aims(xc='LDA', species_dir='/home/mazay/prg/aims/species_defaults/defaults_2020/light').write_input(asi.atoms)
  

lib_file = '/home/mazay/prg/aims/build2/libaims.210513.scalapack.mpi.so'
init_func = init_aims

with DFT_C_API(lib_file, init_func, MPI.COMM_WORLD, molecule('H2O'), 'asi-2h2o-1') as asi:
  asi.run()
  E0 = asi.total_energy
  #ch = asi.atomic_charges
  ch = None
  print (f'E = {E0}, ch = {ch}')
  esp, coords = asi.get_esp()
  print (f'{coords.shape} {esp.shape} {esp[:3]} {coords[:3]}')

  #esp = asi.calc_esp(np.array([[5,5,5],[10,10,10]], dtype=np.float64))
  esp_calc = asi.calc_esp(coords)
  print ('esp_calc = ',esp_calc[:3])
  np.savetxt('esp.txt', np.array([esp, esp_calc]).T)
  err = esp - esp_calc
  max_abs_err = np.max(np.abs(err))
  max_rel_err1 = np.max(np.abs(err/esp))
  max_rel_err2 = np.max(np.abs(err/esp_calc))
  print ('errrs:',max_abs_err, max_rel_err1, max_rel_err2)
  
  

