from mpi4py import MPI

from ctypes import c_double, c_void_p, CFUNCTYPE, cast, py_object, c_int, POINTER
from pyasi import DFT_C_API, init_dftbp, init_aims
import numpy as np
from numpy.testing import assert_allclose
from ase.build import molecule
from ase import units
import os, sys
from time import sleep
from itertools import product

from mpi4py.util.dtlib import from_numpy_dtype


print ('np.dtype(c_double)', np.dtype(c_double).descr)
mpitype_d = from_numpy_dtype(np.dtype(c_double))
mpitype_d.Commit()



@CFUNCTYPE(None, c_void_p, c_int, POINTER(c_double), POINTER(c_double), POINTER(c_double))
def ext_pot_mpi_client(ptr, n, coords_ptr, potential_ptr, potential_grad_ptr):

  srv, weight = cast(ptr, py_object).value
  coords = np.ctypeslib.as_array(coords_ptr, shape=(n, 3))
  tag = 0
  
  if potential_ptr:
    potential = np.ctypeslib.as_array(potential_ptr, shape=(n, ))
    tag += 1
  
  if potential_grad_ptr:
    potential_grad = np.ctypeslib.as_array(potential_grad_ptr, shape=(n, 3))
    tag += 2
  
    
  #print ('Send coords', coords.dtype.descr, coords.shape)
  MPI.COMM_WORLD.Send([coords, mpitype_d], dest=srv, tag=tag)
  #print ('Send coords done!')

  if potential_ptr:
    MPI.COMM_WORLD.Recv([potential, mpitype_d], source=srv, tag=0)
    #potential[:] = 0.0
    potential *= weight

  if potential_grad_ptr:
    MPI.COMM_WORLD.Recv([potential_grad, mpitype_d], source=srv, tag=0)
    potential_grad *= weight
    #potential_grad[:,:] = 0.0
  
  #print ('Received pot &/or grad')
  

def ext_pot_mpi_server(asi):
  while True:
    #print ('Waiting status')
    stat = MPI.Status()
    MPI.COMM_WORLD.Probe(status=stat)
    #print ('Status=',stat)
    n = stat.Get_count() // (3 * np.dtype(np.float64).itemsize)
    tag = stat.Get_tag()
    #print ('tag=',tag, 'n=', n)
    if tag == 99: # exit
      stub = np.zeros(1)
      MPI.COMM_WORLD.Recv(stub)
      break
    else:
      assert tag in [1,2,3]
      coords = np.zeros((n,3), dtype=c_double)
      MPI.COMM_WORLD.Recv([coords, mpitype_d])
      pot, grad = asi.calc_esp(coords)
      #pot = np.zeros((n,), dtype=c_double)
      #grad = np.zeros((n,3), dtype=c_double)
      #print ('Send pot', pot.dtype.descr)
      if tag & 1:
        MPI.COMM_WORLD.Send([pot, mpitype_d], dest=stat.Get_source(), tag=0)
        #print ('Send pot done')
      if tag & 2:
        MPI.COMM_WORLD.Send([grad, mpitype_d], dest=stat.Get_source(), tag=0)
        #print ('Send grad done')


test_cases = {'aims':   ('/home/mazay/prg/aims/build2/libaims.211010.scalapack.mpi.so', init_aims), 
              'dftbp':  ('./asidftbp.so', init_dftbp)}

lib_file, initializer = test_cases[sys.argv[1]]

d = float(sys.argv[2])

h2o1 = molecule('H2O')
h2o2 = h2o1.copy()
h2o2.translate([0, 0, d])

world_rank = MPI.COMM_WORLD.Get_rank()
world_size = MPI.COMM_WORLD.Get_size()
if world_rank == 0:
  color = 0
  key_in_color = 0
  srv = None
else:
  n_workers = world_size - 1
  i_worker = world_rank - 1
  
  pool_size = n_workers // 2
  color = i_worker // pool_size + 1
  key_in_color = i_worker % pool_size
  srv = {1:pool_size + 1, 2:1}[color]
print (f'Split proc #{world_rank}: color={color} key={key_in_color} srv=#{srv}')
comm = MPI.COMM_WORLD.Split(color, key_in_color)

mol = {0:h2o1 + h2o2, 1:h2o1, 2:h2o2}[color]
other_mol = {0:None, 1:h2o2, 2:h2o1}[color]


asi = DFT_C_API(lib_file, initializer, comm, mol, f'asi-{color}')
asi.init()
asi.run()
E0 = asi.total_energy
ch = asi.atomic_charges
print (f'{color}: {E0} {ch}')
E_list = MPI.COMM_WORLD.gather(E0, root=0)
if color == 0:
  assert abs(E_list[1] - E_list[2]) < 1e-8, E_list
  E_bind = E_list[0] - E_list[1]*2
  print ('E_bind = ', E_bind)
MPI.COMM_WORLD.Barrier()

if color == 0:
  asi.close()
else:
  for i, j in product(range (10), [1,2]):
    weight = min(1.0, (i+1.)/3.)
    if j == color:
      asi.close()
      #print ('Client color == ', color, 'comm=', comm)
      asi = DFT_C_API(lib_file, initializer, comm, mol, f'asi-{color}')
      asi.init()
      asi.register_external_potential(ext_pot_mpi_client, (srv, weight) )
      asi.run()
      #print ('Stopping server')
      MPI.COMM_WORLD.Send(np.zeros(1), dest=srv, tag=99) # stop server
      E = asi.total_energy
      print (i,j, f'{weight:.1f}', E, E - E0)
    else:
      ext_pot_mpi_server(asi)
  asi.close()

      

MPI.COMM_WORLD.Barrier()

