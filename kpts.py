from ase.calculators.aims import Aims
from ase.build import molecule
import numpy as np

atoms = molecule('H2O')
atoms.pbc = [True]*3
atoms.cell = np.eye(3) * 10
atoms.center()

Aims(xc='LDA', 
  output=['hirshfeld-I'], 
  sc_accuracy_forces=1e-1, 
  kpts=(3,3,3),
  species_dir='/home/mazay/prg/aims/species_defaults/defaults_2020/light'
).write_input(atoms)


