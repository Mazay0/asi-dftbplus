#define _GNU_SOURCE
#include <dlfcn.h>

#include <iostream>

typedef int (*int_func_t)();

int main()
{
	std:: cout << LM_ID_NEWLM << std::endl;
        std:: cout << RTLD_NOW << std::endl;

   	void *handle = dlmopen(LM_ID_NEWLM, "./asidftbp.so", RTLD_NOW);
        int_func_t fn = reinterpret_cast<int_func_t>(dlsym(handle, "ASI_flavour"));
  
        std::cout << handle << std::endl;
        std::cout << fn() << std::endl;

	return 0;
}
