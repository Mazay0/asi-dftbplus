
# for Intel compiler on Hawk
#AIMS_DIR=/home/c.sacps4/aims/build.so
#AIMS_LIB=-laims.210513.scalapack.mpi
#DFTBP_HOME=/home/c.sacps4/dftbp/lib64 
#DFTBP_LIB_DIR=${DFTBP_HOME}/lib64 
#LIBS='-lmkl_rt'


#AIMS_DIR=${HOME}/prg/aims/build2
AIMS_DIR=${HOME}/prg/aims/build-so-3
#AIMS_LIB=-laims.210513.scalapack.mpi
#AIMS_LIB=-laims.211010.scalapack.mpi
#AIMS_LIB=-laims.220214.scalapack.mpi
AIMS_LIB=-laims.220309.scalapack.mpi
#DFTBP_HOME=${HOME}/opt/dftb+
DFTBP_HOME=${HOME}/opt/dftbp-mpi
DFTBP_LIB_DIR=${DFTBP_HOME}/lib
DFTBP_LIBS=-lscalapack-openmpi -ldftbplus -lmudpack -lscalapackfx -lmpifx -lopenblas
LIBS=-lopenblas

all : test-dftbp test-aims

asi.o : asi.cpp asi.h
	mpicxx -c -std=c++11 -fPIC -I. -I${DFTBP_HOME}/include asi.cpp -o asi.o

asidftbp.so : asi.o
	mpicxx -shared -Wl,-rpath=${DFTBP_LIB_DIR} -L${DFTBP_LIB_DIR} ${DFTBP_LIBS} asi.o -o asidftbp.so

test.o : test.cpp asi.h codespec.hpp
	mpicxx -g -c -std=c++11 -I. test.cpp -o test.o

test_esp.o : test_esp.cpp asi.h codespec.hpp
	mpicxx -g -c -std=c++11 -I. test_esp.cpp -o test_esp.o

make_dms.o : make_dms.cpp asi.h codespec.hpp
	mpicxx -g -c -std=c++11 -I. make_dms.cpp -o make_dms.o

test_expdmhs.o : test_expdmhs.cpp asi.h blacsutils.h
	mpicxx -g -c -std=c++14 -I. test_expdmhs.cpp -o test_expdmhs.o

test_initdm.o : test_initdm.cpp asi.h blacsutils.h
	mpicxx -g -c -std=c++14 -I. test_initdm.cpp -o test_initdm.o

make_config_aims.o : make_config_aims.cpp codespec.hpp
	mpicxx -c -std=c++11 -fPIC -I. make_config_aims.cpp -o make_config_aims.o

make_config_dftbp.o : make_config_dftbp.cpp
	mpicxx -c -std=c++11 -fPIC -I. make_config_dftbp.cpp -o make_config_dftbp.o

test-dftbp : test.o make_config_dftbp.o asidftbp.so
	mpicxx -Wl,-rpath=${DFTBP_LIB_DIR} -Wl,-rpath=. -L${DFTBP_LIB_DIR} -lscalapack-openmpi ${DFTBP_LIBS} make_config_dftbp.o test.o     asidftbp.so -o test-dftbp

make-dms: make_dms.o make_config_dftbp.o asidftbp.so
	mpicxx -Wl,-rpath=${DFTBP_LIB_DIR} -Wl,-rpath=. -L${DFTBP_LIB_DIR} -lscalapack-openmpi ${DFTBP_LIBS} make_config_dftbp.o make_dms.o asidftbp.so -o make_dms

test-aims: test.o make_config_aims.o
	mpicxx -g -Wl,-copy-dt-needed-entries -Wl,-rpath=${AIMS_DIR}/subdirectory_aims1/build -Wl,-rpath=${AIMS_DIR} -L${AIMS_DIR} ${LIBS} ${AIMS_LIB} make_config_aims.o test.o  -o test-aims

#--------------------------------------------------------------------------------------------------

test_esp-dftbp: test_esp.o make_config_dftbp.o asidftbp.so
	mpicxx -Wl,-rpath=${DFTBP_LIB_DIR} -Wl,-rpath=`pwd` -L${DFTBP_LIB_DIR} ${DFTBP_LIBS} make_config_dftbp.o test_esp.o asidftbp.so -o test_esp-dftbp

test_esp-aims: test_esp.o make_config_aims.o
	mpicxx -g -Wl,-copy-dt-needed-entries -Wl,-rpath=${AIMS_DIR}/subdirectory_aims1/build -Wl,-rpath=${AIMS_DIR} -L${AIMS_DIR} ${LIBS} ${AIMS_LIB} make_config_aims.o test_esp.o  -o test_esp-aims

test_expdmhs-dftbp: test_expdmhs.o make_config_dftbp.o asidftbp.so
	mpicxx -Wl,-rpath=${DFTBP_LIB_DIR} -Wl,-rpath=`pwd` -L${DFTBP_LIB_DIR} ${DFTBP_LIBS} test_expdmhs.o asidftbp.so -o test_expdmhs-dftbp

test_expdmhs-aims: test_expdmhs.o
	mpicxx -g -Wl,-copy-dt-needed-entries -Wl,-rpath=${AIMS_DIR}/subdirectory_aims1/build -Wl,-rpath=${AIMS_DIR} -L${AIMS_DIR} ${LIBS} ${AIMS_LIB} test_expdmhs.o  -o test_expdmhs-aims

test_initdm-aims: test_initdm.o
	mpicxx -g -Wl,-copy-dt-needed-entries -Wl,-rpath=${AIMS_DIR}/subdirectory_aims1/build -Wl,-rpath=${AIMS_DIR} -L${AIMS_DIR} ${LIBS} ${AIMS_LIB} test_initdm.o  -o test_initdm-aims


clean : 
	rm *.o asidftbp.so test-dftbp test-aims
