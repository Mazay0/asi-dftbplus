#include <complex>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <asi.h>

#include <mpi.h>

#include <cassert>
#include <cmath>
#include <cstdint>
#include <ctime>


#include "codespec.hpp"
#include "utils.hpp"

#define    DTYPE_             0                   /* Descriptor Type */
#define    CTXT_              1                     /* BLACS context */
#define    M_                 2             /* Global Number of Rows */
#define    N_                 3          /* Global Number of Columns */
#define    MB_                4                 /* Row Blocking Size */
#define    NB_                5              /* Column Blocking Size */
#define    RSRC_              6            /* Starting Processor Row */
#define    CSRC_              7         /* Starting Processor Column */
#define    LLD_               8           /* Local Leading Dimension */
#define    DLEN_              9                 /* Descriptor Length */

extern "C" void blacs_get_(int*, int*, int*);
extern "C" void blacs_gridinit_(int*, char*, int*, int*); // BLACS_GRIDINIT( ICONTXT, ORDER, NPROW, NPCOL )

// https://www.intel.com/content/www/us/en/develop/documentation/onemkl-developer-reference-c/top/scalapack-routines/scalapack-redistribution-copy-routines/p-gemr2d.html
// http://www.netlib.org/scalapack/slug/node164.html
// http://www.netlib.org/scalapack/slug/node168.html
extern "C" void pdgemr2d_(int *m , int *n , double *a , int *ia , int *ja , int *desca , double *b , int *ib , int *jb , int *descb , int *ictxt);
extern "C" void descinit_(int*, int*, int*, int*, int*, int*, int*, int*, int*, int*); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO

extern "C" void blacs_gridinfo_(int *, int *, int *, int *, int *); // blacs_gridinfo (icontxt, nprow, npcol, myrow, mycol);
extern "C" int numroc_(int *n, int *nb, int *iproc, int *isrcproc, int *nprocs);


int mpi_provided_threading, world_size, world_rank;



double homog_pot(void *aux_ptr, double x, double y, double z)
{
  double *hom_field = reinterpret_cast<double*>(aux_ptr);
  double P = -(x * hom_field[0] + y * hom_field[1] + z * hom_field[2]);
  //std::cout << "homog_pot @ "  << x << " " << y << " " << z << " = " << P << std::endl;
  return P;
}

double pnt_pot_1(void *aux_ptr, double x, double y, double z)
{
  double *charge_and_coords = reinterpret_cast<double*>(aux_ptr);
  
  //std::cout << charge_and_coords[0] << " " << charge_and_coords[1] << " " << charge_and_coords[2] << " " << charge_and_coords[3] << std::endl;
  const double chg = charge_and_coords[0];
  const double *coords = &(charge_and_coords[1]);
  const double r = sqrt((x - coords[0])*(x - coords[0]) + (y - coords[1])*(y - coords[1]) + (z - coords[2])*(z - coords[2]));
  const double P = chg / r;
  //std::cout << "pnt_pot @ "  << x << " " << y << " " << z << " = " << P << std::endl;
  return P;
}

void pnt_pot(void *aux_ptr, int n, const double *coords, double *potential, double *potential_grad)
{
  const double d = 0.0001;
  for(int i  = 0; i < n; ++i)
  {
    double p = pnt_pot_1(aux_ptr, coords[i*3 + 0], coords[i*3 + 1], coords[i*3 + 2]);
    if (potential)
    {
      potential[i] = p;
    }
    
    if (potential_grad)
    {
      potential_grad[i*3 + 0] = (pnt_pot_1(aux_ptr, coords[i*3 + 0] + d, coords[i*3 + 1], coords[i*3 + 2]) - p) / d;
      potential_grad[i*3 + 1] = (pnt_pot_1(aux_ptr, coords[i*3 + 0], coords[i*3 + 1] + d, coords[i*3 + 2]) - p) / d;
      potential_grad[i*3 + 2] = (pnt_pot_1(aux_ptr, coords[i*3 + 0], coords[i*3 + 1], coords[i*3 + 2] + d) - p) / d;
    }
  }
}


class shut_cout_t
{
  std::ofstream devnull;
  std::streambuf *coutbuf;
  public:
  shut_cout_t()
  {
    coutbuf = std::cout.rdbuf();
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    if (world_rank !=0 )
    {
      devnull.open("/dev/null");
      std::cout.rdbuf(devnull.rdbuf());
    }
  }
  
  ~shut_cout_t()
  {
    std::cout.rdbuf(coutbuf);
  }
};


void print_enumerated(size_t n, const double *v)
{
  for(size_t i = 0; i < n; ++i)
  {
    std::cout << i << " " << v[i] << std::endl;
  }
}

template<typename T>
void save_txt(const char *fn, size_t n, const T *v)
{
  std::ofstream fo(fn);
  fo << std::setprecision(15);
  fo <<  n << std::endl;
  for(size_t i = 0; i < n; ++i)
  {
    fo << v[i] << "\n";
  }
  fo.close();
}

std::vector<double> load_txt(const char *fn)
{
  std::ifstream fi(fn);
  size_t n, i;
  fi >>  n;
  std::vector<double> v(n);
  for(size_t i = 0; i < n; ++i)
  {
    fi >>  v[i];
  }
  fi.close();
  return v;
}

double maxerr(size_t n, const double *a, const double *b)
{
  double res = 0;
  for(auto i = 0; i < n; ++i)
  {
    res = std::max(res, fabs(a[i] - b[i]));
  }
  return res;
}

int get_system_context(int blacs_context)
{
  int what=10, val;
  blacs_get_(&blacs_context, &what, &val); // get system context of the blacs_context
  return val;
}

int get_default_system_context()
{
  int system_context, zero=0;
  blacs_get_(&zero, &zero, &system_context); // get default system context
  return system_context;
}

int make_matrix_context(int init_context, int MP, int NP)
{
  char order = 'R';
  blacs_gridinit_(&init_context, &order, &MP, &NP);
 
  return init_context;
}

int locrow(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[M_], &desc[MB_], &myrow, &desc[RSRC_], &nprow);
}

int loccol(int *desc)
{
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&desc[CTXT_], &nprow, &npcol, &myrow, &mycol);
  
  return numroc_(&desc[N_], &desc[NB_], &mycol, &desc[CSRC_], &npcol);
}

std::vector<int> make_desc(int M, int N, int ctx)
{
  std::vector<int> desc(DLEN_);
  
  if (ctx  == -1)
  {
    desc[CTXT_] = -1;
    return desc; // stub for non-participating process
  }
  
  int nprow, npcol, myrow, mycol;
  blacs_gridinfo_(&ctx, &nprow, &npcol, &myrow, &mycol);  
  
  int MB=1, NB=1, rsrc=0, csrc=0;
  int zero = 0;
  int info;
  
  int locrM = numroc_(&M, &MB, &myrow, &rsrc, &nprow);
  int lld = std::max(1, locrM);

  descinit_(&desc[0], &M, &N, &MB, &NB,  &rsrc,  &csrc, &ctx,  &lld, &info); // DESC, M, N, MB, NB, IRSRC, ICSRC, ICTXT, LLD, INFO

  return desc;
}

std::vector<double> gather(int *desc, double *data, int out_context)
{
  int N1 = 1;
  const size_t G_size = (out_context != -1) ? desc[M_]*desc[N_] : 0;
  std::vector<double> G(G_size);
  
  auto descG = make_desc(desc[M_], desc[N_], out_context);

  std::cout << "gather @" << world_rank << ": " << desc[CTXT_] << " -> " << descG[CTXT_] << " common: " << desc[CTXT_] << std::endl;
  pdgemr2d_(&desc[M_], &desc[N_], data,     &N1, &N1, desc,
                                  G.data(), &N1, &N1, &descG[0], &desc[CTXT_]);
  std::cout << "gather @" << world_rank << " done"<< std::endl;
  
  return G;
}

void distribute(int M, int N, int *in_desc, double *in_data, int out_context, std::vector<int> &out_desc, std::vector<double> &out_data)
{
  make_desc(M, N, out_context).swap(out_desc);
  
  out_data.resize(locrow(&out_desc[0]) * loccol(&out_desc[0]));
  
  int one = 1;
  std::cout << "distribute @" << world_rank << ": " << in_desc[CTXT_] << " -> " << out_desc[CTXT_] << " common: " << out_desc[CTXT_] << std::endl;
  pdgemr2d_(&M, &N, in_data, &one, &one, in_desc,
              out_data.data(),  &one, &one, &out_desc[0], &out_desc[CTXT_] );
  std::cout << "distribute @" << world_rank << " done"<< std::endl;
}

void load_and_distribute(const char *fn, std::vector<int> &out_desc, std::vector<double> &out_data)
{
  std::cout << "fn " << fn << std:: endl;
  auto data = load_txt(fn);
  std::cout << "data " << data[0] << std:: endl;
  int M = sqrt(data.size());
  int N = M;
  std::cout << "data " << data.size() << " = " <<  M << " x " << N << std:: endl;
  int sys_ctx = get_default_system_context();
  std::cout << "sys_ctx " << sys_ctx << std:: endl;
  int ctx1x1 = make_matrix_context(sys_ctx, 1, 1);
  std::cout << "ctx 1x1 " << ctx1x1 << std:: endl;
  auto desc = make_desc(M, N, ctx1x1);
  
  std::cout << "desc " << desc[CTXT_] << std:: endl;
  int out_ctx = make_matrix_context(sys_ctx, world_size, 1);
  std::cout << "out_ctx " << out_ctx << std:: endl;
  
  distribute(M, N, &desc[0], &data[0], out_ctx, out_desc, out_data);
  
  MPI_Barrier(MPI_COMM_WORLD);
  auto full_dm = gather(&out_desc[0], &out_data[0], ctx1x1);
  MPI_Barrier(MPI_COMM_WORLD);
  
  if (world_rank == 0)
  {
    assert(full_dm.size() == data.size());  
    auto err = maxerr(full_dm.size(), &full_dm[0], &data[0]); 
    std::cout << "maxerr on load_and_distribute @" << world_rank << " = " << err << std::endl;
  }
}

/*

void test_dm_export()
{
  int n_spin;
  int *dm_desc;
  double *dm;
  n_spin = ASI_get_nspin();
  std::cout << world_rank << " n_spin:" << n_spin << std::endl;
  int nkpts = ASI_get_nkpts();
  std::vector<int> local_ks(nkpts*n_spin); //top cap
  int n_local_ks = ASI_get_local_ks(local_ks.data());

  //for (int i_spin = 0; i_spin < n_spin; ++i_spin)
  //for (int i_k=0; i_k < n_local_kpts; ++i_k)
  for (int i_ks=0; i_ks < n_local_ks; ++i_ks)
  {
    const int i_kpnt = local_ks.at(i_ks*2 + 0);
    const int i_spin = local_ks.at(i_ks*2 + 1);
    
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "--------------- spin = " << i_spin << " ------------------"  <<std::endl;
    MPI_Barrier(MPI_COMM_WORLD);
    ASI_get_dm(i_kpnt, i_spin, &dm_desc, &dm);
    //std::cout << world_rank << " DESC:";
    //for (int i = 0; i < 9; ++i) { std::cout << dm_desc[i] << " ";}
    std::cout << std::endl;
  
    std::cout << world_rank << " DM:" << dm[0] << " " << dm[1] << std::endl;
    int dm_ctx = dm_desc[1];
    int dm_sys_ctx = get_system_context(dm_ctx);
    {
      int nprow, npcol, myrow, mycol;
      blacs_gridinfo_(&dm_ctx, &nprow, &npcol, &myrow, &mycol);
      std::cout << world_rank << " dm_ctx = "<< dm_ctx << " nprow, npcol, myrow, mycol:" << nprow << " " << npcol << " " << myrow << " " << mycol << std::endl;
    }
    
    int ctx1x1 = make_matrix_context(dm_sys_ctx, 1, 1);
    {
      int nprow, npcol, myrow, mycol;
      blacs_gridinfo_(&ctx1x1, &nprow, &npcol, &myrow, &mycol);
      std::cout << world_rank << " ctx1x1 = "<< ctx1x1 << " nprow, npcol, myrow, mycol:" << nprow << " " << npcol << " " << myrow << " " << mycol << std::endl;
    }

    auto full_dm = gather(dm_desc, dm, ctx1x1);
    std::cout << " gather done @" <<  world_rank << std:: endl;
    MPI_Barrier(MPI_COMM_WORLD);
    
    if (ctx1x1 != -1)
    {
      save_txt(STR("dm_" << i_kpnt << "_" << i_spin <<".txt").c_str(), full_dm.size(), &full_dm[0]);
      //auto dm = load_txt("dm_init.txt");
      //assert(dm.size() == full_dm.size());
      //const double maxE  =maxerr(dm.size(), &full_dm[0], &dm[0]);
      //std::cout << "DM MaxErr = " <<  maxE << std::endl;
      //assert(maxE < 1e-6);
    }
  }  
}*/

void test_dm_import(std::vector<int> *dm_descs, std::vector<double> *dm_datas)
{
  int *dm_desc_ptrs[2] = {&dm_descs[0][0], &dm_descs[1][0]};
  double *dm_ptrs[2] = {&dm_datas[0][0], &dm_datas[1][0]};
  
  ASI_set_dm(dm_desc_ptrs, dm_ptrs, 1);
}

void s_callback(void *aux_ptr, int *blacs_descr, void *blacs_data)
{
  std::cout << "s_callback @" << world_rank << " = " << locrow(blacs_descr) << " X " << loccol(blacs_descr) << std::endl;
  if (world_rank == 0)
  {
    std::complex<double> *cmplx = reinterpret_cast<std::complex<double> *>(blacs_data);
    save_txt("S.txt", locrow(blacs_descr)*loccol(blacs_descr), cmplx);
  }
}

void dm_callback(void *aux_ptr, int iK, int iS, int *blacs_descr, void *blacs_data)
{
  std::cout << "dm_callback @" << world_rank << " = " << locrow(blacs_descr) << " X " << loccol(blacs_descr) << std::endl;
  if (world_rank == 0)
  {
    std::complex<double> *cmplx = reinterpret_cast<std::complex<double> *>(blacs_data);
    save_txt("DM.txt", locrow(blacs_descr)*loccol(blacs_descr), cmplx);
  }
}

int main(int argc, char *argv[])
{
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_provided_threading); // instead of MPI_Init
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
 // shut_cout_t shut_cout_instance;
 
  assert(argc == 2);
  bool pbc;
  if (std::string(argv[1]) == "pbc")
  {
    pbc = true;
  }
  else if (std::string(argv[1]) == "nopbc")
  {
    pbc = false;
  }
  else
  {
    assert(false);
  }
 
  std::vector<int> dm_init_desc[2];
  std::vector<double> dm_init_data[2];
  load_and_distribute("dm_0_init.txt", dm_init_desc[0], dm_init_data[0]);
  load_and_distribute("dm_1_init.txt", dm_init_desc[1], dm_init_data[1]);
  
  MPI_Barrier(MPI_COMM_WORLD);

  
  const MPI_Fint f_mpi_comm = MPI_Comm_c2f(MPI_COMM_WORLD);

  make_config_files(pbc);
  
  ASI_init("asi.log", f_mpi_comm);
  std::cout << "Number of atoms == " << ASI_n_atoms() << std::endl;
  std::cout << "ASI_get_nspin == " << ASI_get_nspin() << std::endl;
  std::cout << "ASI_get_nkpts == " << ASI_get_nkpts() << std::endl;
  std::cout << "ASI_is_hamiltonian_real == " << ASI_is_hamiltonian_real() << std::endl;
  std::cout << "ASI_get_basis_size == " << ASI_get_basis_size() << std::endl;
  std::cout << "ASI_get_n_local_ks == " << ASI_get_n_local_ks() << std::endl;
  
  std::vector< int > local_ks(ASI_get_n_local_ks() * 2);
  ASI_get_local_ks(local_ks.data());
  std::cout << "local_ks @" << world_rank << " : ";
  for(int ks:local_ks)
  {
    std::cout << ks << " ";
  }
  std::cout << std::endl;
  
  ASI_register_overlap_callback(s_callback, 0);
  ASI_register_dm_callback(dm_callback, 0);
  
  double coords[] = {	1, 0, -1,
			1,  0.78306400, 0,
			1, -0.78306400, 0};
  for (auto &x : coords)
  {
    x /= 0.52917721;
  }
  ASI_set_atom_coords(coords, 3);
  double esp_field[3] = {0, 0.01, 0};
  double pnt_charge_and_coords[4] = {1.0, 1, 0, 2.0};


  if (not pbc)
  {
    //ASI_set_external_potential(homog_pot, esp_field);
    //ASI_register_external_potential(homog_pot, esp_field);
    ASI_register_external_potential(pnt_pot, pnt_charge_and_coords);
    //ASI_set_external_potential(pnt_pot, pnt_charge_and_coords);
  }
  
  if (not pbc)
  {
    test_dm_import(dm_init_desc, dm_init_data);
  }

  std::cout << "ASI_run() ..." << std::endl;
  auto t0 = rdtsc();
  ASI_run();
  std::cout << "ASI_run() DONE!" << std::endl;
  std::cout << "ASI_get_nkpts == " << ASI_get_nkpts() << std::endl;

  t0 = rdtsc();
  auto E = ASI_energy();
  std::cout << std::setprecision(6) << std::fixed;
  std::cout << "Energy == " << E  << " Ha = " << E * 27.2113845 << " eV" << std::endl;
  
  const double *forces = ASI_forces();
  std::cout << "Forces == ";
  for (size_t i = 0; i < 3 * ASI_n_atoms(); ++i )
    std::cout << forces[i] << " ";
  std::cout << std::endl;
  

  const double *atomic_charges = ASI_atomic_charges();
  std::cout << "atomic_charges == ";
  for (size_t i = 0; i < ASI_n_atoms(); ++i )
    std::cout << atomic_charges[i] << " ";
  std::cout << std::endl;

  if (not pbc)
  {
    double esp_coords[2*3] = { 10, 10, 10,    1,  2,  0}; 
    double esp[2];
    
    ASI_calc_esp(2, esp_coords, esp, 0);
    
    std::cout << "EP1=" << esp[0] << " = " << esp[0] * 27.2113845 << " V" << std::endl;
    std::cout << "EP2=" << esp[1] << " = " << esp[1] * 27.2113845 << " V"  << std::endl;
    
    assert_all(E, forces, atomic_charges, esp);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  /*
  std::vector<int> local_ks(ASI_get_nkpts()*ASI_get_nspin());
  int n_local_ks = ASI_get_local_ks(local_ks.data());
  std::cout << "n_local_ks = " << n_local_ks  << " @ " << world_rank << std::endl;
  for (auto ki: local_ks)
  {
    std::cout << "ki in local_ks = " << ki << " @ " << world_rank << std::endl;
  }
  
  test_dm_export();*/
  
  ASI_finalize(); 
  MPI_Finalize();
  return 0;
}
