from ase.calculators.aims import Aims
from ase import Atoms
from ase.build import molecule
import os

os.environ['AIMS_SPECIES_DIR'] = '/home/mazay/prg/aims/species_defaults/defaults_2020/light'
os.environ['ASE_AIMS_COMMAND'] = '/home/mazay/prg/aims/build-exe/aims.211010.scalapack.mpi.x'



atoms = Atoms('O2', 
  pbc=False,
  positions = [[ 0.      ,  0.      ,  0.622978],       [ 0.      ,  0.      , -0.622978]])

calc  = Aims(xc='pw-lda')
atoms.calc = calc
print (atoms.get_potential_energy())

