from ase.calculators.aims import Aims
from ase import Atoms
import os

os.environ['AIMS_SPECIES_DIR'] = '/home/mazay/prg/aims/species_defaults/defaults_2020/light'
os.environ['ASE_AIMS_COMMAND'] = '/home/mazay/prg/aims/build-exe/aims.211010.scalapack.mpi.x'


atoms = Atoms('Si2', 
  pbc=True,
  positions = [[0,0,0],[0.13567730000E+01, 0.13567730000E+01, 0.13567730000E+01]],
  cell = [[0.27135460000E+01,   0.27135460000E+01,   0.00000000000E+00], 
          [0.00000000000E+00,   0.27135460000E+01,   0.27135460000E+01], 
          [0.27135460000E+01,   0.00000000000E+00,   0.27135460000E+01]])

calc  = Aims(k_grid = [2,2,2], xc='pw-lda')

atoms.calc = calc


atoms.get_potential_energy()

