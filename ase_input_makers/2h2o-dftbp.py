from ase import Atoms
from ase.build import molecule
import os

from ase.calculators.dftb import Dftb




atoms = Atoms('OHHOHH', 
  pbc=False,
  positions = [[ 0.      ,  0.      ,  0.119262],   [ 0.      ,  0.763239, -0.477047],   [ 0.      , -0.763239, -0.477047], 
               [ 5.      ,  0.      ,  0.119262],   [ 5.      ,  0.763239, -0.477047],   [ 5.      , -0.763239, -0.477047]])

calc = Dftb(label='H2O_cluster', slako_dir='./',
      Hamiltonian_SCC='Yes',
      Hamiltonian_MaxAngularMomentum_='',
      Hamiltonian_MaxAngularMomentum_O='"p"',
      Hamiltonian_MaxAngularMomentum_H='"s"')
      
calc.write_input(atoms)
atoms.calc = calc
print (atoms.get_potential_energy())

